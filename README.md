# dotfiles
Dotfiles for everything what I use

Mirrored from https://github.com/sprucecellodev125/dotfiles

Available for:
sway, waybar, xfce4-panel, Hyprland

Requirements:
Xfce: Plank, Catppuccin-light GTK theme, Papirus Icon theme, vala-panel-appmenu
Sway/Wayland: wlogout, waybar

Credits:
[Catppuccin](https://github.com/catppuccin)
